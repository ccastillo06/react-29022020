import React, { useState } from 'react';

import SingleFile from './components/SingleFile';
import MultipleFile from './components/MultipleFile';

import './App.css';

function App() {
  const [isSingleFile, setIsSingleFile] = useState(true);

  return (
    <div className="App">
      <button type="button" onClick={() => setIsSingleFile(!isSingleFile)}>
        Change to {isSingleFile ? 'MULTIPLE' : 'SINGLE'} file upload
      </button>
      {isSingleFile ? <SingleFile /> : <MultipleFile />}
    </div>
  );
}

export default App;
